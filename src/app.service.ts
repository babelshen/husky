import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Lalalalal!';
  }
  getAnotherHello(): string {
    return 'Hello!!';
  }
}
